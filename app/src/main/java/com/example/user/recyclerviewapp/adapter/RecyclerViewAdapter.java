package com.example.user.recyclerviewapp.adapter;


import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.user.recyclerviewapp.R;
import com.example.user.recyclerviewapp.activity.DetailScreenActivity;
import com.example.user.recyclerviewapp.constants.AppConstants;
import com.example.user.recyclerviewapp.model.MyColleageModel;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {
    private Context context;
    private List<MyColleageModel> list;

    public RecyclerViewAdapter(Context context, List<MyColleageModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Bitmap bMap = BitmapFactory.decodeFile("res/drawable/" + list.get(position).getImageName());
        holder.thumbnail.setImageBitmap(bMap);
        holder.title.setText(list.get(position).getTitle());
        holder.thumbnail.setImageResource(getImageByName(list.get(position).getImageName()));
        holder.cardView.setOnClickListener(v ->{
            Intent intent = new Intent(v.getContext(), DetailScreenActivity.class);
            intent.putExtra(AppConstants.DETAIL_INTENT_KEY, list.get(position));
            v.getContext().startActivity(intent);
        });

    }

    private int getImageByName(String imageName) {
        Resources resources = context.getResources();
        final int resourceId = resources.getIdentifier(imageName, "drawable",
                context.getPackageName());
        return resourceId;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        ImageView thumbnail;
        TextView title;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardViewId);
            thumbnail = itemView.findViewById(R.id.thumbnailId);
            title = itemView.findViewById(R.id.titleId);
        }
    }

}