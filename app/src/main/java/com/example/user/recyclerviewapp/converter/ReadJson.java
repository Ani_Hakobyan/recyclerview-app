package com.example.user.recyclerviewapp.converter;

import android.content.Context;
import android.widget.Toast;

import com.example.user.recyclerviewapp.constants.AppConstants;

import java.io.IOException;
import java.io.InputStream;

public class ReadJson {
    public static String loadJSONFromAsset(Context context, String langunage){
        String json = null;
        String jsonFile;
        try {
            if (langunage.equals(AppConstants.EMGLISH)){
                jsonFile = AppConstants.JSON_FILE_ENGLISH;
            } else {
                jsonFile = AppConstants.JSON_FILE_ARMENIA;
            }
            InputStream is = context.getAssets().open(jsonFile);

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {

            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
