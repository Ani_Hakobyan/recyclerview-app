package com.example.user.recyclerviewapp.model;

import java.io.Serializable;

public class MyColleageModel implements Serializable {

    private int id;
    private String title;
    private String text;
    private String imageName;

    public MyColleageModel(int id, String title, String text, String imageName) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.imageName = imageName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
