package com.example.user.recyclerviewapp.activity;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import com.example.user.recyclerviewapp.adapter.RecyclerViewAdapter;
import com.example.user.recyclerviewapp.constants.AppConstants;
import com.example.user.recyclerviewapp.model.MyColleageModel;
import com.example.user.recyclerviewapp.R;
import com.example.user.recyclerviewapp.converter.ReadJson;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{
    private RecyclerView recyclerView;
    private String langunage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        langunage = AppConstants.ARMENIA;
        initAdapter(converJsonToObject(ReadJson.loadJSONFromAsset(this, langunage)));

    }

    private List<MyColleageModel> converJsonToObject(String jsonBody) {
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<MyColleageModel>>() {
        }.getType();
        List<MyColleageModel> modelList = gson.fromJson(jsonBody, type);

        return modelList;
    }

    private void initAdapter(List<MyColleageModel> modelList) {
        LinearLayoutManager linearLayoutManager;
        RecyclerViewAdapter adapter;
        SwipeRefreshLayout layout;

        recyclerView = findViewById(R.id.recyclerViewID);

        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        layout = findViewById(R.id.swipe_refreshLayoutID);
        layout.setOnRefreshListener(this);

        runAnimation(recyclerView);

        adapter = new RecyclerViewAdapter(this, modelList);
        recyclerView.setAdapter(adapter);
        dragAndDropItems(modelList,adapter);
        layout.setRefreshing(false);


    }

    private void dragAndDropItems(List<MyColleageModel> modelList,RecyclerViewAdapter adapter) {

        ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP|ItemTouchHelper.DOWN,0) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder dragged, @NonNull RecyclerView.ViewHolder target) {
                int position_dragged = dragged.getAdapterPosition();
                int position_target = target.getAdapterPosition();

                Collections.swap(modelList,position_dragged,position_target);
                adapter.notifyItemMoved(position_dragged,position_target);

                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

            }
        });
        helper.attachToRecyclerView(recyclerView);
    }

    private void runAnimation(RecyclerView recyclerView) {
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_slide_from_left);
        recyclerView.setLayoutAnimation(controller);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.armenianLanguage:
                langunage = AppConstants.ARMENIA;
                initAdapter(converJsonToObject(ReadJson.loadJSONFromAsset(this, langunage)));
                return true;

            case R.id.englishLanguage:
                langunage = AppConstants.EMGLISH;
                initAdapter(converJsonToObject(ReadJson.loadJSONFromAsset(this, langunage)));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onRefresh() {
        initAdapter(converJsonToObject(ReadJson.loadJSONFromAsset(this, langunage)));
    }

}

