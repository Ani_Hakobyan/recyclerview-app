package com.example.user.recyclerviewapp.constants;

public interface AppConstants {
    // intent key
    String DETAIL_INTENT_KEY = "detail_intent_key";
    // language
    String ARMENIA = "AM";
    String EMGLISH = "EN";
    // json files name
    String JSON_FILE_ENGLISH = "enChristmas.json";
    String JSON_FILE_ARMENIA = "armChristmas.json";
}
