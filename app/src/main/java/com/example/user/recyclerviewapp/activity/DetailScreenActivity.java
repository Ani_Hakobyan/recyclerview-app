package com.example.user.recyclerviewapp.activity;


import android.content.Intent;
import android.content.res.Resources;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.user.recyclerviewapp.R;
import com.example.user.recyclerviewapp.constants.AppConstants;
import com.example.user.recyclerviewapp.model.MyColleageModel;

public class DetailScreenActivity extends AppCompatActivity {
    // view
    private ImageView image;
    private TextView title, text;
    // object
    private MyColleageModel colleageModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_screen);

        toolBar();
        initViews();
        getIntentData(getIntent());
    }

    private void toolBar(){
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }
    private void initViews() {
        runAnimation();
        image = findViewById(R.id.detailImageId);
        text = findViewById(R.id.detailDescriptionId);
        title = findViewById(R.id.detailTitleId);
    }

    private void runAnimation() {
        AppBarLayout appBarLayout;
        LinearLayout linearLayout;
        LayoutAnimationController controller = null;
        appBarLayout = findViewById(R.id.appBarLayoutID);
        linearLayout = findViewById(R.id.LinearLayoutID);

        controller = AnimationUtils.loadLayoutAnimation(this,R.anim.layout_slide_from_bottom);
        linearLayout.setLayoutAnimation(controller);

        controller = AnimationUtils.loadLayoutAnimation(this,R.anim.layout_fall_down);
        appBarLayout.setLayoutAnimation(controller);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }


    private void getIntentData(Intent intent) {
        if (intent != null) {
            colleageModel = (MyColleageModel) intent.getSerializableExtra(AppConstants.DETAIL_INTENT_KEY);
            if (colleageModel != null) {
                setContentInViews(colleageModel);
            }
        }
    }

    private void setContentInViews(MyColleageModel colleageModel) {
       image.setImageResource(getImageByName(colleageModel.getImageName()));
       text.setText(colleageModel.getText());
       title.setText(colleageModel.getTitle());

    }

    private int getImageByName(String imageName) {
        Resources resources = getResources();
        final int resourceId = resources.getIdentifier(imageName, "drawable", getPackageName());
        return resourceId;
    }
}
